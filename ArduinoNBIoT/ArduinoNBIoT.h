#ifndef _ARDUINO_NB_IOT_H
#define _ARDUINO_NB_IOT_H

const char menu_cmd_1[] PROGMEM = "HELP"; const char menu_dsc_1[] PROGMEM = "print this menu";
const char menu_cmd_2[] PROGMEM = "TIME"; const char menu_dsc_2[] PROGMEM = "print current time";
const char menu_cmd_3[] PROGMEM = "AT"; const char menu_dsc_3[] PROGMEM = "ok or not";
const char menu_cmd_4[] PROGMEM = "AT+NRB"; const char menu_dsc_4[] PROGMEM = "reboot";
const char menu_cmd_5[] PROGMEM = "AT+NMGR"; const char menu_dsc_5[] PROGMEM = "read message";
const char menu_cmd_6[] PROGMEM = "AT+CSQ"; const char menu_dsc_6[] PROGMEM = "signal intensity";
const char menu_cmd_7[] PROGMEM = "AT+NUESTATS=ALL"; const char menu_dsc_7[] PROGMEM = "all info";
const char menu_cmd_8[] PROGMEM = "AT+NMSTATUS?"; const char menu_dsc_8[] PROGMEM = "CDP status";
const char menu_cmd_9[] PROGMEM = "AT+NMGS=2,CAFE"; const char menu_dsc_9[] PROGMEM = "send message for test";
const char menu_cmd_10[] PROGMEM = "AT+NMGS=10,030101FFFEFDFCAEAEA0"; const char menu_dsc_10[] PROGMEM = "send long message for test";
const char menu_cmd_11[] PROGMEM = "AT+CGATT?"; const char menu_dsc_11[] PROGMEM = "network register";
const char menu_cmd_12[] PROGMEM = "AT+NNMI=1"; const char menu_dsc_12[] PROGMEM = "set read message immediately";
const char menu_cmd_13[] PROGMEM = "AT+NNMI=2"; const char menu_dsc_13[] PROGMEM = "set show message received";
const char menu_cmd_14[] PROGMEM = "AT+S"; const char menu_dsc_14[] PROGMEM = "save and reboot";
const char menu_cmd_15[] PROGMEM = "AT+CIMI"; const char menu_dsc_15[] PROGMEM = "query sim card IMSI";
const char menu_cmd_16[] PROGMEM = "AT+CGPADDR=?"; const char menu_dsc_16[] PROGMEM = "query ip address";
const char menu_cmd_17[] PROGMEM = "AT+CCLK?"; const char menu_dsc_17[] PROGMEM = "query network time";
const char menu_cmd_18[] PROGMEM = "AT+CPSMS?"; const char menu_dsc_18[] PROGMEM = "query PSM save power mode";
const char menu_cmd_19[] PROGMEM = "AT+NQMGR"; const char menu_dsc_19[] PROGMEM = "statistics for incoming message";
const char menu_cmd_20[] PROGMEM = "AT+NQMGS"; const char menu_dsc_20[] PROGMEM = "statistics for sent message";
const char menu_cmd_21[] PROGMEM = "AT+NCDP=117.60.157.137,5683"; const char menu_dsc_21[] PROGMEM = "set usr ip and port";
const char menu_cmd_22[] PROGMEM = "AT+NCDP?"; const char menu_dsc_22[] PROGMEM = "query ip and port";
const char menu_cmd_23[] PROGMEM = "AT+NCCID?"; const char menu_dsc_23[] PROGMEM = "query sim card ICCID";
const char menu_cmd_24[] PROGMEM = "AT+NCHIPINFO=ALL"; const char menu_dsc_24[] PROGMEM = "query temperature and battery";
const char menu_cmd_25[] PROGMEM = "AT+BUILD"; const char menu_dsc_25[] PROGMEM = "firmware build time";
const char menu_cmd_26[] PROGMEM = "AT+VER"; const char menu_dsc_26[] PROGMEM = "firmware version";
const char menu_cmd_27[] PROGMEM = "AT+PDTIME"; const char menu_dsc_27[] PROGMEM = "hardware production time";
const char menu_cmd_28[] PROGMEM = "AT+SN"; const char menu_dsc_28[] PROGMEM = "hardware serial number";
const char menu_cmd_29[] PROGMEM = "AT+CGSN=1"; const char menu_dsc_29[] PROGMEM = "query IMEI";

// const char menu_cmd_XX[] PROGMEM = ""; const char menu_dsc_14[] PROGMEM = "";

const char * const commands[][2] PROGMEM = {
  menu_cmd_1, menu_dsc_1,
  menu_cmd_2, menu_dsc_2,
  menu_cmd_3, menu_dsc_3,
  menu_cmd_4, menu_dsc_4,
  menu_cmd_5, menu_dsc_5,
  menu_cmd_6, menu_dsc_6,
  menu_cmd_7, menu_dsc_7,
  menu_cmd_8, menu_dsc_8,
  menu_cmd_9, menu_dsc_9,
  menu_cmd_10, menu_dsc_10,
  menu_cmd_11, menu_dsc_11,
  menu_cmd_12, menu_dsc_12,
  menu_cmd_13, menu_dsc_13,
  menu_cmd_14, menu_dsc_14,
  menu_cmd_15, menu_dsc_15,
  menu_cmd_16, menu_dsc_16,
  menu_cmd_17, menu_dsc_17,
  menu_cmd_18, menu_dsc_18,
  menu_cmd_19, menu_dsc_19,
  menu_cmd_20, menu_dsc_20,
  menu_cmd_21, menu_dsc_21,
  menu_cmd_22, menu_dsc_22,
  menu_cmd_23, menu_dsc_23,
  menu_cmd_24, menu_dsc_24,
  menu_cmd_25, menu_dsc_25,
  menu_cmd_26, menu_dsc_26,
  menu_cmd_27, menu_dsc_27,
  menu_cmd_28, menu_dsc_28,
  menu_cmd_29, menu_dsc_29,
};

#endif // _ARDUINO_NB_IOT_H
